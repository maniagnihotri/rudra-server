Rudra
=====

####Rudra - an adaptable web server. 
Rudra is a light weight web server designed for handling high volumes of upload and download requests. It has the capability to be adaptable to 

 - available memory (detailed in RunModes Section) and 
 - CPU usage ( detailed in Dynamic Threading Section).

It uses Apache Maven for builds, unit testing and integration testing. The server supports *GET,POST,PUT,HEAD* and *OPTIONS* methods OOTB in the current version.

##### Disclaimer
Please contact mani0119@gmail.com for source & binaries. Above license is valid for personal and informational usage. Additionally, for commercial usage a commercial license is required.

Requirements
-------------

 - Rudra is a Java based application and requires a **JRE 1.7** [minimum version] installation to run.
 - Recommended to use *minimum* of *512 MB* of memory when running this server. This can be specified via the JVM option -Xms512m. If heap memory allocated to the server is less than 256 MB, optimization for performance are restricted (but not disabled). The server is fully functional even with lower memory allocation and few performance optimizations might still be carried out (depending on the runmode).
 - **Maven 2.2.1** [minimum version]


Features
------------

- GET, HEAD, POST, PUT and OPTIONS methods are implemented OOTB in the current version. Methods can be implemented/customized easily by adding implementation classes and corresponding method name mapping in the configurations file.
- HTML and JSON response formats are supported OOTB. The architecture allows, easy addition/customization of formats.
- Persistent connections support for clients on HTTP 1.1, allowing multiple requests to be served over a single http (socket) connection. Connections keep-alive behavior is available for HTTP 1.0 clients which support keep-alive.
- Customizable UI. For a request to list the contents of a directory, the server sends a configured HTML file. Changing the UI, simply needs the HTML (or the resource set used in the HTML) to be updated. A fallback HTML response may also be generated, if the HTML file configuration does not exist or is inaccessible. Moreover, if the client specifies a response format in the request, the request is delegated to the corresponding response builder and response is generated aptly.
- Supports HTTP 1.0 and some important features of HTTP 1.1 (details in the #Limitations section). 
- As a security measure, allows requests to be served from content only under root directory (as configured) for public usage. Server UI content is kept separately and is also accessible in read-only mode to clients. 
- Access logging which logs the requested URL, method, response code/error and file (if any) sent to the client.
- Remote Monitoring via JMX, to monitor load on server, update configurations, change runmode of running server etc.
- 


Highlights
-----------

Rudra has several unique features, some important one's are listed below : 

##### _Architecture: Loosely coupled implementation_

 - Most of the implementation is de-coupled from the core implementation and hooked via configurations file. This loose coupling of implementation classes, makes customization much more easier (details in #Customization Section).

##### _Architecture: Plug and Play model_

 - The server is open to new enhancements and has an extendible plug and play model. New classes/improvements can be plugged in. To name a few :
	 - Filters to filter requests and response can be plugged in via configurations. These filters are applied in the order they are defined in the configuration.
	 - New request methods may be plugged in simply via configurations.
	 - Response formats are pluggable. OOTB JSON and HTML are provided. A new format support can be added by adding a corresponding response builder.  

##### _Performance: RunModes_

Server also supports various runmodes optimised for different usage patterns. They can help bring down the runtime costs (majorly when server is operated on shared resources like shared setups/VMs/AWS).

##### _Performance: Dynamic Threading_

Apart from run modes, server can be configured (alongwith) to use dynamic threading. This enables a more lenient thread creation approach, in which server creates more threads (only when load is high) to improve the response time. Threads are attempted to be created judiciously and they are reused once created. Of course, they never exceed the max thread limit configured in any case (detailed in #'Dynamic Threading' section)

##### _Reliability: Optimized Listener_

The Server implementation takes off responsibilities like addition of tasks work queue and sending rejection responses, away from the Server class. The sole purpose of the Server class is to listen for requests. All the related processing tasks are handled by a small group of threads called as acceptor threads. In worst case when request has to be rejected, there are workers to take care of it. The main server thread is never blocked on this. This way making the server more reliable and responsive to requests. 
 
##### _Monitoring: JMX Remote Management_

Important parameters and management operations are exposed via JMX. These can help in analysing system state, reloading configurations, purging cancelled tasks, switching runmode, changing load factor etc. These can help tweak the server to improve performance.
 


Customizations
---------------

Following can be customized by updating the configuration:

- UI can be customized by simply changing the UI elements like 'css', 'js' and the 'html' files.
- Functionalities that can be customized:
	- `IReader interface implementation`. The Request Reader reads underlying channel and provides data mostly in character based (String) format.
	- `IMultiPartReader interface implementation`. The MultiPart reader reads raw data from the underlying channel and provides binary data blocks to the caller. This is mostly suited when data to be read has a high possibility of being binary data. It can however read data in character based (String) format as well.
	- `IResponseBuilder interface implementation`. The Response Builders own the responsibility of building response content in their exclusive format. Different response builders can be configured for different format of responses desired. The server looks up for a apt builder mapped to a particular format when client sends a formatKey parameter in request. This key is also configurable and defaults to ‘format’.
	- `AbstractHeaders class implementation`. The Headers parsing mechanism can be customized based on need.
	- `ICache interface implementation`. This class will be responsible for caching files on server (limited by the maximum cache size configuration) and providing performance improvement.
	- `AbstractRequestHandler class implementation`. Changing the OOTB implementation class for existing handler will alter the execution mechanism.
	- The execution mechanism of existing request handlers can also be customized by altering the handler specific properties.
- The max cache size set to 20 MB by default & max size of the Request has been set to 100 MB in configurations and can be customized if needed.



Extensibility
--------------

 - Apart from the existing OOTB handlers for different request methods, new handlers for unimplemented request methods can also be added. When the request method is extracted from the request data, a corresponding handler class name is looked up. If the method matches with any of the method names in the configurations (only for the first hit of the said method, after which it is cached), the corresponding handler is instantiated and request processing is handed over to the handler. This extensible model allows new request methods to be added very easily.
 - Further more, instead of having fixed set of properties, each request handler can define its own set of properties which are propagated back to handler instances.
 - Request and Responses can also be altered using filter pre & post processing of request and responses respectively. Filters can perform pre and post processing of request just like filters in most of the web/app servers. The order of the filter definition is maintained when they are applied. 
  ![Request Filtering flow](readme_images/request_filtering.jpg)
 

RunModes
---------------

Server can be run in different run modes, each having their own benefits and trade offs. The runmodes can help improvise the performance of the server. The 

####vanilla 

 - Also the default run-mode if no run-mode is specified. The server runs with constant parameters as mentioned in the config and never deviates from them.
 - This is the closet to having constant memory usage even on peak load with delta changes when load peaks constantly. The rate of change however is small, relatively.
 - This mode is suited more, when few authors are connected to the system and are uploading large number of files frequently but the size of the files being handled is small w.r.t the read and write buffer sizes with mostly constant load. For example, if files of size 100KB on average are being handled and the read/write buffers are set to 100KB, then this mode performs well. There is no toggling of memory/keep-alive and so memory consumption is more or less constant for consistent load.
 - This runmode is also better suited for instances where server is required to be used more like a publish instance, which has more GET/HEAD requests, rather than POST/PUT requests.

####beast 

 - The server run with constant parameters in this mode as well similar to vanilla mode. However, they are not the same as specified in the config, but a superlative form of them. 
 - Parameters are transformed by the supplied transform factor at the start of the server (setting the transform factor to 1, disables the parameter adjustment).
 - Even then, if the load is excessive, keep-alive timeout is minimized to resolve more requests rather than waiting for requests on a connected channel, and disabled on extended extreme load in this mode. This behavior returns back to normal when the pending requests come back under achievable range.
 - This runmode is suited if the average file size server has to deal with are relatively large with varying load on server. For example if the average file size is 20MB and configured read/write buffers are 100KB with a transform factor of 10, the request will be read off in a **20 iterations**, as compared to vanilla mode which would take **200 iterations** to read the same file.
 - This is also suited where the instance is to be used mostly for uploading (PUT/POST) content. The performance is better but as a trade off there is more memory usage (only when server is under high load).

####resilient

 - The server runs with toggling parameters, and keeps altering them based on load, processing queue length and some other factors to determine which mode the server should be running in.
 - In this mode, server keeps toggling between the 'Beast' mode and 'Vanilla' mode on need basis.
 - Server reacts to requests load more dynamically. To help galvanise the stress reduction, server switches to beast mode to process requests at a faster pace (during this time server uses more memory). If the incoming request count does not reduce considerably the HTTP Keep-alive is minimized and finally disabled in this mode, when server is under severe extended stress. This, however is temporary and keep-alive is enabled back when stress reduces on the server. This brings down the pseudo-active thread count involved in maintaining keep-alive behavior (and technically are idle), allowing resources to be shared with the more needy processes. 
 - This runmode is suited for instances where the deviation from the average file size is huge, or if the instance is used as publish (GET) and  as an author for uploading (POST/PUT) content, both. The performance of this runmode lies inbetween 'vanilla' and 'beast' with more optimized memory usage as compared to 'beast'.

>_Note:_
	If server is running with a low max heap memory upper bound (max heap memory available <256 MB) and has a runmode enabled which toggles the read and write buffer sizes (which would increase the memory consumption) as well. In such a case, these parameters are not toggled and the runmode features are restricted. The same is also intimated at the server startup.
	In essence, only the parameters which do not directly affect the memory usage are toggled (keep-alive time/count).



Dynamic Threading
-----------------

 - Dynamic Threading improves the response time without impacting the keep-alive time of each request. 
 - It changes the way new worker threads are created in the server. Instead of having only initial (core) worker threads to process all requests even when pending requests are more than what core worker threads can handle at the moment, dynamic threading creates few new threads to increase the request processing speed based on requirement. 
 - Threads **are not** created on each consecutive request. A thread is added only if there are no idle threads available. Additionally created threads, become idle after processing the request and hence next requests do not create new threads, but are processed with the existing ones. The response time improves many folds with this.
 - Albeit, the improvement in response time, this mechanism utilizes more 
  - CPU, proportional to the increase in threads and 
  - memory, proportional to the increase in threads.
 - This mechanism can be combined with other run-modes. It performs pretty well with 'vanilla' run-mode itself. The performance is superlative when resilient and beast run-modes are used.



Server Performance Comparison
------------------------------

Server running with 512MB of total memory, was bench marked against an 1000 uploads of a 4KB image each with 10 users, each sending 100 requests, separated by 1 second of think time and 100 GET requests of same image with 1 second think time in total. 

- #Samples: As defined this is the total number of requests sent to the server
- Average: The average time taken to resolve a request.
- Median: The most common amount of time taken to resolve the request.
- 90% line: 90% samples took no more than this much time.
- 95% line: 95% samples took no more than this much time.
- 99% line: time (at most) taken by 99% of samples. Alternatively, only 1% samples took time more than this.
- Min: Minimum time taken by a request to send response.
- Max: The maximum time any of the request took to respond back with the response.
- Error%: The percentage of samples which did not return a successful response.
- Throughput: It is the no of requests resolved per second. But since we are not stressing the server to it's full capacity (error is 0%), this is capped by the think time and the number of requests per second. The point is with the different runmodes the number varies significantly.

`Comparison results for different Runmodes, vanilla, resilient, beast and Dynamic Threading (with vanilla runmode).`

>![Vanilla](readme_images/vanilla.jpg)
>![Resilient](readme_images/resilient.jpg)
>![Beast](readme_images/beast.jpg)
>![Vanilla with DynamicThreading](readme_images/vanilla-dynamicThreaded.jpg)



Setting up
------------

The setup requires basic understanding of maven, which is the build tool for this server source code. With reference to the current installation, maven is used to:

* compile, 
* unit test, 
* integration test (optional) and
* package contents for distribution.

The build should work with any maven version greater than 2.2.1 (assembly plugin dependency constraint).


To set up the source, extract the Rudra-WebServer folder provided in the distribution zip (Rudra-WebServer-package.zip) and navigate into it.

##### Command line:

To compile via commandline open a command prompt and navigate to the Rudra-WebServer folder.

- To run the **Unit Tests** execute:
```bash
mvn clean test 
```
-For **skipping** the unit tests, while installing, execute:

```bash
mvn clean install -Dskip.unit.tests=true
```
A similar property `skip.integration.tests` also exists for skipping integration tests. Integration tests however, are not run by default as the said property is set to `True` by default.

- To run **Integration Tests**, execute:

```bash
mvn clean verify -P integration-test
```
The profile integration-test simply sets the property `skip.integration.tests` to `False` and `skip.unit.tests` to `True`.

##### Eclipse:

To setup code base in eclipse:

1. Navigate to File -> Import Project
2. Select Maven Project -> Existing Maven Projects
3. Browse to the 'pom.xml' in the unzipped distribution folder and choose it.
4. Click Next -> Finish
5. Wait for the project import to finish.


Starting the Server
--------------------

#####Running via Command line

1. Open a Terminal/Command prompt and navigate to the unzipped folder path.
2. Navigate to the bin folder which contains the `Rudra-WebServer.jar`, `start.sh` and `start.bat` files. 

a). _Running on Unix_: change the permissions of the start.sh file to have executable rights before it can be run. Execute the following:
```bash
chmod +x start.sh
./start.sh <SERVER_STARTUP_OPTIONS>
``` 

b). _Running on Windows_: Execute the start.bat by double clicking on it or running it from the command line as:
```bash
start.bat <SERVER_STARTUP_OPTIONS>
```

>_Note:_ adding the help directive (-h or --help) to the start command will display help menu. Adding help directive after a startup option will display help for that option. For example start.bat -runmode -h will display all the valid options for runmodes and their significance. 

#####Running in Eclipse

1. Select the Project and click on 'Run' -> Run As Application.
2. Select the 'main' file from the list 'org.rudra.main.ServerManager' and click OK.


####Advanced Options
Server can be started without using the helper start up script files. This way allowing users to tweak the runtime parameters at a more granular level.

- JVM parameters can be customized, JMX remote monitoring can be enabled/disabled and of course the server startup parameters can be tuned just as it can be done from the helper start scripts.

The Java command can be executed as:
```bash
java <JVM_OPTIONS> -jar Rudra-WebServer.jar <SERVER_STARTUP_OPTIONS>
```

Some important parameters are discussed below:
- One of the important JVM_OPTION is heap memory parameter
	- adding -Xms512m sets an minimum lower bound of available memory to 512 MB and -Xmx1g sets the upper bound to 1GB.
- Remote Monitoring via JMX, add the following parameters to the server startup command, where 1617 is the port exposed for JMX clients (this is just for illustration).
	
		> -Dcom.sun.management.jmxremote \
		> -Dcom.sun.management.jmxremote.port=1617 \
		> -Dcom.sun.management.jmxremote.authenticate=false \
		> -Dcom.sun.management.jmxremote.ssl=false \


Limitations/Assumptions
-----------------------

 - Mime type of a file is determined based on the file extension and only for configured mime types, corresponding mime types are sent in response, for others fall back (application/octet-stream) is used.
 - Status messages can be generated only for configured Status Codes. For others, only Status Codes will be sent. More status code and status message combinations can be added in the config if need be.
 - User Interface (GUI) is minimalistic and hasn't been tested in detail. It has been sanitized in Firefox (45.0.2). Users are encouraged to develop/test their own UI which can be used to navigate and update the server content.
 - Invalid/contradictory configuration parameters validation is limited. Parameters are expected to be valid as described in comments for each parameter in configuration file and users are advised to read the documentation for valid values of parameters. They are validated against an XSD for parameter type.
 - Features like 100-Continue, If-* conditions, Range* requests are not supported in the current version. 
 - As per the RFC differences [2], OPTIONS method is limited to HTTP 1.1 and is not available for HTTP 1.0 Clients.
 



Logging
------------

Based on performance and feature evaluation from

	 - http://www.java-logging.com/comparison/  and  
	 - https://www.loggly.com/blog/benchmarking-java-logging-frameworks/

 - log4j 1.2.17 was chosen for logging.
 - Access logging is done separately. These access log files have "access"  appended to their filename. The Request Id, requested URL, response code, response representation/information are captured along with the timestamp in the access log.
 - The configurations for logging are controlled via "log4j.properties" file which should either be available in the classpath or else should be placed along with the server-config.xml.



References
---------------
1. [HTTP RFC 2616](https://www.ietf.org/rfc/rfc2616.txt)
2. [HTTP Versions Comparison](http://www8.org/w8-papers/5c-protocols/key/key.html)
3. [CopyRight Comparison](https://opensource.com/law/13/1/which-open-source-software-license-should-i-use)
4. [Copyright management](https://yoxos.eclipsesource.com/yoxos/node/org.eclipse.releng.tools.feature.group)
5. [IDE: Spring Source Tool Suite](https://spring.io/tools/sts)
6. [ReadMe Editor](https://stackedit.io)
7. [Java Logging comparison 1](http://www.java-logging.com/comparison/)
8. [Java Logging comparison 2](https://www.loggly.com/blog/benchmarking-java-logging-frameworks/)
9. [Stackoverflow: for general issues.](http://stackoverflow.com/)



 -------------------------------------------------
Rudra:- The mightiest of the mighty ([wikipedia](https://en.wikipedia.org/wiki/Rudra))